const cheerio = require('cheerio');
const dateFns = require('./utils/date')
const { format } = require('date-fns')
const { readFile } = require('./pupeteerFns')
const fs = require('fs');

const result = (uptoDate = null) => {
  uptoDate = uptoDate === null ? new Date() : new Date(uptoDate);
  uptoDate = format(uptoDate, 'yyyy-MM-dd');
  let result = {
    data: [
      /**
       * {
       *    'title' : "",
       *    'url' : "",
       *    'price' : "",
       *    'date' : ""
       * }
       */
    ],
    continueMore: true
  };
  try {
    const text = readFile();
    const $ = cheerio.load(text);

    const attributes = [
      {
        'attr': 'style',
        'value': 'line:height:130%;'
      },
      // {
      //   'attr': 'border',
      //   'value': '0'
      // },
      // {
      //   'attr': 'align',
      //   'value': 'center'
      // },
      // {
      //   'attr': 'cellpadding',
      //   'value': '0'
      // },
      // {
      //   'attr': 'cellspacing',
      //   'value': '0'
      // }
    ];

    $('td').each(function () {
      let currentElement = $(this);
      let status = true;
      for (let i = 0; i < attributes.length; i++) {
        element = attributes[i];
        if (currentElement.attr(element.attr) == element.value) {
          status = true;
        } else {
          status = false;
          break;
        }
      }

      if (status) {
        let tr = currentElement.parent();
        let temp = {
          url: null,
          price: null,
          date: null,
          title: null
        }

        temp.url = tr.find('td:nth-child(3) a').attr('href');
        temp.title = tr.find('td:nth-child(3) a font').text().trim();
        temp.date = tr.find('td:nth-child(4)').text().trim();
        temp.price = tr.find('td:nth-child(5)').text().trim().replace(/\D/g, "");
        let tempDate = dateFns.createDateObjectFromFormat(temp.date);
        if (dateFns.checkIfDateOneGreaterThanOrEqualToDateTwo(tempDate, uptoDate)) {
          result.data.push(temp);
        } else {
          result.continueMore = false;
          //return result;
        }
      }
    });

  } catch (err) {
    console.error(err)
  }
  //console.log(result);
  return result;
}

//console.log(result());

module.exports = {
  result
}




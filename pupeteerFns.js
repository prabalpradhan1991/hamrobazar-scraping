const puppeteer = require('puppeteer');
const fs = require('fs');

const configureBrowser = async (url) => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();

  await page.goto(url, {
    waitUntil: 'networkidle2',
  });
  return { page, browser };
}

const getSelector = async (page, condition) => {
  let selector = await page.$$(condition);
  return selector;
}

const checkCheckbox = async (page, selector) => {
  await page.waitForSelector(selector);
  const element = await page.$(selector);
  const isChecked = await (await element.getProperty("checked")).jsonValue();
  if (!isChecked) {
    await page.click(selector);
  }
}

async function scrape (page) {
  await page.reload();
  let html = await page.evaluate(() => document.body.innerHTML);
  return html;
}

async function writeToFile (html) {
  await fs.writeFileSync('./storage/scrape.html', html, err => {
    if (err) {
      console.error(err)
      return
    }
  })
}

const readFile = () => {
  const text = fs.readFileSync('./storage/scrape.html', 'utf8')
  return text;
}

module.exports = {
  scrape,
  checkCheckbox,
  getSelector,
  configureBrowser,
  writeToFile,
  readFile
}
const { listing } = require('./listing');
const { configureBrowser } = require('./pupeteerFns');
const cheerio = require('cheerio');
const { Parser } = require('json2csv');
const fs = require('fs');
const prompt = require('prompt-sync')({ sigint: true });
const { format, subDays } = require('date-fns')

let result = [];

(async () => {
  const priceFrom = prompt('Enter From price:(10000) ');
  const priceTo = prompt('Enter To price:(100000) ');
  const include8gb = prompt('Include 8gb (y for yes):(y) ');
  const include12gb = prompt('Include 12gb or more (y for yes):(y) ');
  const uptoDate = prompt(`Adds not older than (Y-m-d):(2days before) `);

  const parameters = {
    priceFrom: priceFrom.length ? parseInt(priceFrom).toString() : '10000',
    priceTo: priceTo.length ? parseInt(priceTo).toString() : '100000',
    include8gb: include8gb.length ? (include8gb == 'y' ? true : false) : true,
    include12gb: include12gb.length ? (include12gb == 'y' ? true : false) : true,
    uptoDate: uptoDate.length ? uptoDate : format(subDays(new Date, 2), 'yyyy-MM-dd')
  }

  console.log(parameters);
  console.log('Scraping....')
  const listings = await listing('https://hamrobazaar.com/c31-mobile-and-accessories-handsets?catid=31&order=siteid&way=0', parameters)
  console.log(`${listings.length} results found...`)
  console.log(`Filtering results for warranty..`);
  for (i = 0; i < listings.length; i++) {
    console.log(`Checking result ${i + 1} for warranty..`);
    let { page, browser } = await configureBrowser(`https://hamrobazaar.com/${listings[i].url}`);
    let el = await page.$x("//tr/td[text() = 'Warranty Period:']")
    if (el.length == 1) {
      el = await el[0].getProperty('parentNode');
      let innerHTML = await (await el.getProperty('innerHTML')).jsonValue();
      let $ = cheerio.load(`<table>${innerHTML}</table>`);
      let warranty = $('td:nth-child(2)').text();
      let temp = { ...listings[i], url: `https://hamrobazaar.com/${listings[i].url}`, warranty }
      result.push(temp);
    }
    await browser.close()
  }
  console.log(`${result.length} found with warranty`)
  console.log(`Generating CSV`)
  if (result.length >= 1) {
    try {
      const parser = new Parser({
        delimiter: ','
      });
      const csv = parser.parse(result);
      fs.writeFileSync('./storage/result.csv', csv, err => {
        if (err) {
          console.error(err)
          process.exit(1);
        }
      })
    } catch (err) {
      console.error(err);
    }
    process.exit(0);
  } else {
    fs.writeFileSync('./storage/result.csv', '', err => {
      if (err) {
        console.error(err)
        process.exit(1);
      }
    })
    console.log('No result found');
  }
  process.exit(1);
})();
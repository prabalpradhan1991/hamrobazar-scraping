const dateFns = require('date-fns')


//date1 and date2 are strings in format Y-m-d
const checkIfDateOneGreaterThanOrEqualToDateTwo = (date1, date2) => {
  date1 = new Date(date1);
  date2 = new Date(date2);
  return dateFns.isAfter(date1, date2) || dateFns.isEqual(date1, date2);
}

const createDateObjectFromFormat = (date, format = 'dmY', seperator = '-') => {
  let dateString = '';
  let year = null;
  let month = null;
  let day = null;

  switch (format) {
    case 'dmY':
      dateString = date.split(seperator);
      year = parseInt(dateString[2]);
      day = parseInt(dateString[0]);
      month = parseInt(dateString[1]) - 1;
      break;
  }
  const dateObject = new Date(year, month, day);
  return dateFns.format(dateObject, 'yyyy-MM-dd');

}

module.exports = {
  checkIfDateOneGreaterThanOrEqualToDateTwo,
  createDateObjectFromFormat
}
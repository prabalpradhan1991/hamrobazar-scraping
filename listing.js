const scrapedData = require('./scrape');
const { getSelector, configureBrowser, checkCheckbox, scrape, writeToFile } = require('./pupeteerFns');

const listing = async (url, parameters = {}) => {
  let result = [];
  const { page, browser } = await configureBrowser(url);

  const selector = await getSelector(page, `select[name="e_2"]`);
  await page.waitForSelector(`select[name="e_2"]`);
  await page.select(`select[name="e_2"]`, '1');

  await page.waitForSelector('#price_from');
  await page.type('#price_from', '');
  await page.type('#price_from', parameters.priceFrom);

  await page.waitForSelector('#price_to');
  await page.type('#price_to', '');
  await page.type('#price_to', parameters.priceTo);

  await page.waitForSelector('#moredes_label28');
  await page.click('#moredes_label28');

  if (parameters.include8gb) {
    await checkCheckbox(page, '#e_2813');
  }

  if (parameters.include12gb) {
    await checkCheckbox(page, '#e_2814');
  }

  await page.waitForSelector(`input[value="Apply Filter"]`);
  await page.click(`input[value="Apply Filter"]`);
  await page.waitForNavigation({ waitUntil: 'networkidle2' });
  //await page.reload();

  let html = await scrape(page);
  await writeToFile(html);

  let continueMore = true;
  do {
    html = await scrape(page);
    await writeToFile(html);
    let resultFromScrapedData = scrapedData.result(parameters.uptoDate);
    let data = resultFromScrapedData.data;
    continueMore = resultFromScrapedData.continueMore;

    result = [...result, ...data];

    let el = await page.$x("//a/b/u[text() = 'Next']")

    if (el.length == 1) {
      el = await el[0].getProperty('parentNode');
      el = await el.getProperty('parentNode');
      if (continueMore) {
        await el.click("//a/b/u[text() = 'Next']");
        await page.waitForNavigation({ waitUntil: 'networkidle2' });
        //await page.reload();
      }
    } else {
      continueMore = false;
    }
  } while (continueMore);

  await browser.close();
  return result;
};

module.exports = {
  listing
}
## Description
Node application to scrape, hamrobazar handsets data

## Requirements
- node v12.19.0
- npm 6.14.9

## Installation process
- npm install
- node index.js
- the application prompts for criterias to scrape. If pressed enter, the application assigns sensible defaults
- after the data is scraped, the result is stored as csv in /storage/result.csv